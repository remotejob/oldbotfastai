import os,sys
from shutil import copyfile
from fastai.text import *

Config.DEFAULT_CONFIG = {
        'data_archive_path': '/kaggle/working/data',
        'data_path': '/kaggle/working/data',
        'model_path': '/kaggle/working/models'
    }
Config.create('/kaggle/working/myconfig.yml')
Config.DEFAULT_CONFIG_PATH = '/kaggle/working/myconfig.yml'
os.environ["FASTAI_HOME"] = "/kaggle/working"
print(os.environ["FASTAI_HOME"])

path = untar_data(URLs.IMDB_SAMPLE)
print(path)

copyfile('../input/cleanoldfinbot.csv',path/'texts.csv')


df = pd.read_csv(path/'texts.csv')
print(df.head())

data_lm = TextLMDataBunch.from_csv(path, 'texts.csv')
# Classifier model data
data_clas = TextClasDataBunch.from_csv(path, 'texts.csv', vocab=data_lm.train_ds.vocab, bs=32)


data_lm.save('data_lm_export.pkl')
data_clas.save('data_clas_export.pkl')


data_lm = load_data(path, 'data_lm_export.pkl')
data_clas = load_data(path, 'data_clas_export.pkl', bs=16)


learn = language_model_learner(data_lm, AWD_LSTM, drop_mult=0.5,pretrained=False)
learn.fit_one_cycle(50, 1e-2)


learn.unfreeze()
learn.fit_one_cycle(50, 1e-3)

# print(learn.predict("This is a review about", n_words=100))

learn.save_encoder('ft_enc')

learn = text_classifier_learner(data_clas, AWD_LSTM, drop_mult=0.5,pretrained=False)
learn.load_encoder('ft_enc')

# print(data_clas.show_batch())

learn.fit_one_cycle(50, 1e-2)
learn.freeze_to(-51) #learn.freeze_to(-2)?
learn.fit_one_cycle(50, slice(5e-3/2., 5e-3))


input_sentences = []

filepath = '../input/inputsentences.txt'  
with open(filepath) as fp:  
    line = fp.readline()
    while line:
        input_sentences.append(line.strip())
        line = fp.readline()
       
with open('txtout.txt', 'a') as file:
    for sent in input_sentences:
        # output_words = learn.predict(sent,n_words=20)
        # file.write(output_words+'\n')
        classres = learn.predict(sent)
        file.write(sent+'\n')
        file.write(str(classres))
        file.write('\n')
        file.write('-----------------------\n')    
         


